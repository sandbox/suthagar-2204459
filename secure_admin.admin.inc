<?php

/**
 * @file
 * Provide all the configuartion for secure admin module
 */

/**
* Page callback: Secure Admin settings
*
* @see secure_admin_menu()
*/
function secure_admin_form($form, &$form_state) {
	$module_path = drupal_get_path('module', 'secure_admin');
	drupal_add_js("$module_path/secure_admin.js");
	global $base_url;
	$form = array();

	$form['secure_admin_siteurl'] = array(
			'#type' => 'textfield',
			'#title' => t('Site Base URL'),
			'#default_value' => variable_get('secure_admin_siteurl', $base_url),
			'#size' => 100,
			'#maxlength' => 200,
			'#description' => t('Site Base URL'),
			'#required' => TRUE,
			'#attributes' => array('readonly' => 'readonly'),
	);

	$form['secure_admin_basepath'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Secure Admin URL'),
	    '#default_value' => variable_get('secure_admin_basepath', 'http://'),
	    '#size' => 100,
	    '#maxlength' => 200,
	    '#description' => t('Seperate Admin URL for handing admin actions. if main url is example.com then admin url like admin.example.com or different domain'),
	    '#required' => TRUE,
    );

	$form['secure_admin_enable'] = array(
		'#type' => 'radios',
		'#title' => t('Enable Secure Admin'),
		'#default_value' => variable_get('secure_admin_enable', 0),
		'#options' => array(t('Disabled'), t('Enabled')),
		'#description' => t('To configure Secure Admin this setting must be enabled.'),
	);

	$form['secure_admin'] = array(
		'#type' => 'fieldset',
		'#title' => t('Secure Admin settings'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);

    $form['secure_admin']["secure_admin_restrict_urls"] = array(
	    "#title" => t("List URLs, one per line"),
	    "#type" => "textarea",
	    "#default_value" => variable_get("secure_admin_restrict_urls", ''),
	    "#description" => t("Some common restrict urls's:
                        <br> Node Add<b>node/add</b>
	    				<br> Dashboard: <b>admin/dashboard</b>
                        <br> Content List <b>admin/content</b>
    					<br> Modules <b>admin/modules</b>
                        <br><br>
                        <b>use * for wildcard</b>
                        "),
  	);

   $form['secure_admin']["secure_admin_ignore_urls"] = array(
   		"#title" => t("List URLs, one per line"),
   		"#type" => "textarea",
   		"#default_value" => variable_get("secure_admin_ignore_urls", ''),
   		"#description" => t("Some common ignore urls's:
                        <br> Secure admin: <b>admin/config/system/secure_admin</b>
                        "),
   );

   $form['secure_admin']['secure_admin_logging'] = array(
   		'#type' => 'checkbox',
   		'#title' => t('Enable Logging'),
   		'#default_value' => variable_get('secure_admin_logging', 0),
   		'#description' => t('Turn on logging to log the details if some one tried to access restricted pages.'),
   );

   $form['secure_admin_httpauth_status'] = array(
   		'#type' => 'checkbox',
   		'#title' => t('Enable Http Authentication'),
   		'#default_value' => variable_get('secure_admin_httpauth_status', 0),
   		'#description' => t('Turn on Http Authentication for restricted pages.'),
   );

   $form['secure_admin_httpauth_filedset'] = array(
   		'#type' => 'fieldset',
   		'#title' => t('Http Authentication settings'),
   		'#collapsible' => TRUE,
   		'#collapsed' => TRUE,
   );

   $form['secure_admin_httpauth_filedset']['secure_admin_httpauth_allow_cli'] = array(
   		'#type' => 'checkbox',
   		'#title' => t('Allow command line access'),
   		'#description' => t('When the site is accessed from command line (e.g. from Drush, cron), the shield should not work.'),
   		'#default_value' => variable_get('shield_allow_cli', 1),
   );

   $form['secure_admin_httpauth_filedset']['credentials'] = array(
   		'#type' => 'fieldset',
   		'#title' => t('Http Authentication Credentials'),
   );

   $form['secure_admin_httpauth_filedset']['credentials']['secure_admin_httpauth_user'] = array(
   		'#type' => 'textfield',
   		'#title' => t('User'),
   		'#default_value' => variable_get('secure_admin_httpauth_user', ''),
   		'#description' => t('Live it blank to disable authentication.')
   );

   $form['secure_admin_httpauth_filedset']['credentials']['secure_admin_httpauth_pass'] = array(
   		'#type' => 'password',
   		'#title' => t('Password'),
   		'#default_value' => variable_get('secure_admin_httpauth_pass', ''),
   );

   $form['secure_admin_httpauth_filedset']['secure_admin_httpauth_message'] = array(
   		'#type' => 'textfield',
   		'#title' => t('Http Authentication message'),
   		'#description' => t("The message to show in the authentication request popup."),
   		'#default_value' => variable_get('secure_admin_httpauth_message', 'Hi User please enter user name and password.'),
   );


   $form['#validate'] = array('secure_admin_form_validate');
   $form['#submit'] = array('secure_admin_form_submit');
   return system_settings_form($form);
}


/**
 * Validate callback for secure_admin_form().
 */
function secure_admin_form_validate(&$form, &$form_state) {
	global $base_url;
	$secure_admin_basepathValidate = FALSE;

	$secure_admin_siteurl = variable_get('secure_admin_siteurl', $base_url);

	// validate secure admin base path
	if($form_state['values']['secure_admin_basepath'] == $secure_admin_siteurl){
		form_set_error('secure_admin_basepath', 'Secure Admin URL should not same as <b>Base Path</p>');
	}elseif (substr($form_state['values']['secure_admin_basepath'], 0,7) == 'http://' || substr($form_state['values']['secure_admin_basepath'], 0,8) == 'https://'){
		$secure_admin_basepathValidate = TRUE;
	}

	if (!$secure_admin_basepathValidate) {
		form_set_error('secure_admin_basepath', 'Secure Admin URL is not valid');
	}

	// validate secure admin settings enabled
	if($form_state['values']['secure_admin_enable'] == 1 && strlen($form_state['values']['secure_admin_basepath']) < 9){
		form_set_error('secure_admin_basepath', 'Secure Admin URL is required. Please enter valid Secure Admin URL.');
	}


	// validate HTTP AUTH settings
	if($form_state['values']['secure_admin_httpauth_status'] == 1){

		// validate HTTP AUTH username
		if($form_state['values']['secure_admin_httpauth_user'] == ''){
			form_set_error('secure_admin_httpauth_user', 'Please enter HTTP Authentication Username.');
		}elseif (strlen($form_state['values']['secure_admin_httpauth_user']) < 6){
			form_set_error('secure_admin_httpauth_user', 'Please enter valid HTTP Authentication Username. Minimum length 6');
		}

		// validate HTTP AUTH password
		if($form_state['values']['secure_admin_httpauth_pass'] == ''){
			form_set_error('secure_admin_httpauth_pass', 'Please enter HTTP Authentication Password.');
		}elseif (strlen($form_state['values']['secure_admin_httpauth_pass']) < 6){
			form_set_error('secure_admin_httpauth_pass', 'Please enter valid HTTP Authentication Password. Minimum length 6');
		}
	}

}

/**
 * Submit callback for secure_admin_form().
 */
function secure_admin_form_submit() {
	// The page cache needs to be cleared after changing settings.
	cache_clear_all('*', 'cache_page', TRUE);
}
