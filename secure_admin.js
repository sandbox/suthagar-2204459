/**
 * @file secure_admin.js
 */
(function ($) {
	Drupal.secureAdmin = Drupal.secureAdmin || {};
	Drupal.behaviors.secureAdmin = {
			attach: function(context, settings) {
				// call init to do default actions.
		        $('form#secure-admin-form', context).once('secureAdmin', Drupal.secureAdmin.init);

		      	$('#edit-secure-admin-enable').once('secureAdmin', function() {

			    	// bind click function for Secure admin enable raio button
		      		$('#edit-secure-admin-enable input.form-radio').click(function(){
		      			Drupal.secureAdmin.showHideSecureAdminSettingsFiledSet();
			        });

			    	// bind click function for Secure admin HTTP Status checkbox
			        $('input#edit-secure-admin-httpauth-status').click(function(){
			        	Drupal.secureAdmin.showHideHttpAuthenticationSettingsFiledSet();
			        });

			        // bind click function for Secure admin HTTP Status checkbox
			        $('#edit-submit.form-submit').click(function(){

			        });

		      	});
	    }
	};

	Drupal.secureAdmin.init = function() {
		Drupal.secureAdmin.showHideSecureAdminSettingsFiledSet();
		Drupal.secureAdmin.showHideHttpAuthenticationSettingsFiledSet();
	};


	Drupal.secureAdmin.showHideSecureAdminSettingsFiledSet = function() {
		var secureAdminEnableVal = $('#edit-secure-admin-enable input.form-radio:checked').val();

	 	if(secureAdminEnableVal == 1){
	 		if($('fieldset#edit-secure-admin .fieldset-wrapper').is(':hidden')){
	 			$('fieldset#edit-secure-admin .fieldset-wrapper').show();
	 		}
	 	}
	};

	Drupal.secureAdmin.showHideHttpAuthenticationSettingsFiledSet = function() {
			var secureAdminHttpauthStatusVal = $('#secure-admin-form input#edit-secure-admin-httpauth-status').val();
		 	if(secureAdminHttpauthStatusVal == 1){
		 		if($('fieldset#edit-secure-admin-httpauth-filedset .fieldset-wrapper').is(':hidden')){
		 			$('fieldset#edit-secure-admin-httpauth-filedset .fieldset-wrapper').show();
		 		}
		 	}
	};

})(jQuery);
